#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "FS.h"

ESP8266WebServer server(80);

void mainLoadRoute()
{
	SPIFFS.begin();
	File f = SPIFFS.open("/index.html", "r");
	server.streamFile(f, "text/html");
	int filesize = f.size();
	SPIFFS.end();

	String WebString = "";
	WebString += "HTTP/1.1 200 OK\r\n";
	WebString += "Content-Type: text/html; charset=utf-8\r\n";
	WebString += "Content-Length: " + String(filesize) + "\r\n";
	WebString += "\r\n";
	server.send(200, "text/html", WebString);
}

void patternLoadRoute(const char *patternFilename)
{
	SPIFFS.begin();
	File f = SPIFFS.open(patternFilename, "r");
	server.streamFile(f, "image/png");
	int filesize = f.size();
	SPIFFS.end();

	String WebString = "";
	WebString += "HTTP/1.1 200 OK\r\n";
	WebString += "Content-Type: image/png\r\n";
	WebString += "Content-Length: " + String(filesize) + "\r\n";
	WebString += "\r\n";
	server.send(200, "text/html", WebString);
}

void patternLoad1Route()
{
	patternLoadRoute("/pattern_1.png");
}

void patternLoad2Route()
{
	patternLoadRoute("/pattern_2.png");
}

void patternLoad3Route()
{
	patternLoadRoute("/pattern_3.png");
}

void patternLoad4Route()
{
	patternLoadRoute("/pattern_4.png");
}

void patternLoad5Route()
{
	patternLoadRoute("/pattern_5.png");
}

void patternAction1Route()
{
}

void patternAction2Route()
{
}

void patternAction3Route()
{
}

void patternAction4Route()
{
}

void patternAction5Route()
{
}

void notFoundRoute()
{
	String message = "File Not Found\n\n";
	message += "URI: ";
	message += server.uri();
	message += "\nMethod: ";
	message +=(server.method() == HTTP_GET) ? "GET" : "POST";
	message += "\nArguments: ";
	message += server.args();
	message += "\n";

	for(uint8_t i = 0; i < server.args(); i++) {
		message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
	}

	server.send(404, "text/plain", message);
}

void setup(void)
{
	Serial.begin(115200);
	WiFi.begin(ssid, password);

	Serial.println("");

	// Wait for connection
	while(WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println("");
	Serial.print("Connected to ");
	Serial.println(ssid);
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());

	if(MDNS.begin("esp8266")) {
		Serial.println("MDNS responder started");
	}

	server.on("/", mainLoadRoute);

	server.on("/data/pattern_1.png", patternLoad1Route);
	server.on("/data/pattern_2.png", patternLoad2Route);
	server.on("/data/pattern_3.png", patternLoad3Route);
	server.on("/data/pattern_4.png", patternLoad4Route);
	server.on("/data/pattern_5.png", patternLoad5Route);

	server.on("/pattern/1", patternAction1Route);
	server.on("/pattern/2", patternAction2Route);
	server.on("/pattern/3", patternAction3Route);
	server.on("/pattern/4", patternAction4Route);
	server.on("/pattern/5", patternAction5Route);

	server.onNotFound(notFoundRoute);

	server.begin();
}

void loop(void)
{
	server.handleClient();
}
