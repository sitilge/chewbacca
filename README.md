# Chewbacca

An ESP8266 based webserver and a web-app.

## Requirements

- Wifi
- WifiClient
- WebServer
- FS
- mDNS (optional)

## Setup

Choose your board from arduino board manager. Follow the instruction given in [the FS plugin website](https://github.com/esp8266/arduino-esp8266fs-plugin) to install the filesystem and flash / link files under the `data` directory. From this point on, the development of the server and the app becomes easy because of the actaul filesystem concept underneath. By default, the app uses the HTTP/1.1 protocol (port 80) to communicate with the server and the static resource (images) requests are routed internally by the server. 

## Result

If you've set it up correctly, 5 default robot emotions should be received when making a request to the respective IP (or FQDN). When clicking on the emotions, respective AJAX requests are made to the server. The default internal AJAX routes are intentionally empty placeholders - it is up to the end-user to define them.
